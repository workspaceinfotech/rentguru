//
//  SignupViewController.swift
//  RentGuru
//
//  Created by Workspace Infotech on 8/2/16.
//  Copyright © 2016 Workspace Infotech. All rights reserved.
//

import UIKit




import DropDown
import Alamofire
import ObjectMapper
import Toast_Swift
import Photos
class SignupViewController: UIViewController ,UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    @IBOutlet var viewUnderIdentyLabel: UIView!
    @IBOutlet var identityLabel: UILabel!
    @IBOutlet var dropDownCotainer: UIView!
    @IBOutlet var passwordTxt: UITextField!
    @IBOutlet var viewUnderPassword: UIView!
    @IBOutlet var viewUnderEmail: UIView!
    @IBOutlet var emailTxt: UITextField!
    @IBOutlet var viewUnderLastname: UIView!
    @IBOutlet var lastNameTxt: UITextField!
    @IBOutlet var viewUnderFirstname: UIView!
    @IBOutlet var firstNameTxt: UITextField!
    let imagePicker = UIImagePickerController()
    let dropDown = DropDown()
    var isDropDownHidden = true
    var image :UIImage? = UIImage()
    var filename : String!
    let defaults = NSUserDefaults.standardUserDefaults()
    var baseUrl : String = ""
    var identityTypeList = [IdentityType]()
    var selectedIdentityIndex  = 0;
    var singupMsg : String = ""
    var presentWindow : UIWindow?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        baseUrl = defaults.stringForKey("baseUrl")!
        presentWindow = UIApplication.sharedApplication().keyWindow
        
        imagePicker.delegate = self
        
        self.emailTxt.delegate = self
        self.firstNameTxt.delegate = self
        self.lastNameTxt.delegate = self
        self.passwordTxt.delegate = self
        
        self.navigationController!.navigationBar.barTintColor = UIColor(netHex:0x2D2D2D)
        
        let button = UIButton(type: UIButtonType.Custom) as UIButton
        button.setImage(UIImage(named: "back-arrow"), forState: UIControlState.Normal)
        button.addTarget(self, action:#selector(SignupViewController.backToLogin), forControlEvents: UIControlEvents.TouchUpInside)
        button.frame=CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        let gesture = UITapGestureRecognizer(target: self, action:#selector(SignupViewController.showHideDropDown) )
        self.dropDownCotainer.addGestureRecognizer(gesture)
        
    }
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
        self.viewUnderEmail.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.6)
        self.viewUnderFirstname.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.6)
        self.viewUnderLastname.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.6)
        self.viewUnderPassword.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.6)
        self.viewUnderIdentyLabel.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.6)
        
        
        self.emailTxt.attributedPlaceholder = NSAttributedString(string: "Email", attributes:[NSForegroundColorAttributeName : UIColor.lightGrayColor()])
        self.firstNameTxt.attributedPlaceholder = NSAttributedString(string: "First Name", attributes:[NSForegroundColorAttributeName : UIColor.lightGrayColor()])
        self.lastNameTxt.attributedPlaceholder = NSAttributedString(string: "Last Name", attributes:[NSForegroundColorAttributeName : UIColor.lightGrayColor()])
        self.passwordTxt.attributedPlaceholder = NSAttributedString(string: "Password", attributes:[NSForegroundColorAttributeName : UIColor.lightGrayColor()])
        self.identityLabel.attributedText  =  NSAttributedString(string: "Identity Type", attributes:[NSForegroundColorAttributeName : UIColor.lightGrayColor()])
        
        
        self.getIdentityTypes()
        
        print(self.identityTypeList.count)
        
        // The view to which the drop down will appear on
        dropDown.anchorView = self.dropDownCotainer // UIView or UIBarButtonItem
        
        // The list of items to display. Can be changed dynamically
        // dropDown.dataSource = ["Car", "Motorcycle", "Truck"]
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            // print("Selected item: \(item) at index: \(index)")
            self.identityLabel.attributedText  =  NSAttributedString(string: (item), attributes:[NSForegroundColorAttributeName : UIColor.whiteColor()])
            self.selectedIdentityIndex = self.identityTypeList[index].id
        }
        dropDown.backgroundColor = UIColor.blackColor()
        dropDown.textColor = UIColor.whiteColor().colorWithAlphaComponent(1)
        dropDown.selectionBackgroundColor = UIColor(netHex:0xD0842D)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillDisappear(animated: Bool)
    {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBarHidden = true
    }
    
    
    func backToLogin() {
        self.performSegueWithIdentifier("backToLogin", sender: nil)
        
    }
    func showHideDropDown()  {
        dropDown.show()
        
    }
    @IBAction func chooseimageAction(sender: AnyObject) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    
    func isValidEmail(testStr:String) -> Bool {
        print("validate emilId: \(testStr)")
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluateWithObject(testStr)
        return result
    }
    func getIdentityTypes(){
        
        Alamofire.request(.GET, NSURL(string: "\(baseUrl)api/utility/get-identity" )!, parameters: [:])
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                switch response.result {
                case .Success(let data):
                    let idenRes: IdentityTypeResponse = Mapper<IdentityTypeResponse>().map(data)!
                    if((idenRes.responseStat.status) != false){
                        self.identityTypeList = idenRes.responseData
                        var animDictionary = Array<String>() 
                        for i in 0 ..< self.identityTypeList.count{
                            animDictionary.append(self.identityTypeList[i].name)
                        }
                        self.dropDown.dataSource = animDictionary
                    }
                case .Failure(let error):
                    print(error)
                }
        }
    }
    
    
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            //                    imageView.contentMode = .ScaleAspectFit
            //                    imageView.image = pickedImage
            print("picked image :\(pickedImage)")
            self.image = pickedImage
        }
        if let imageURL = info[UIImagePickerControllerReferenceURL] as? NSURL {
            let result = PHAsset.fetchAssetsWithALAssetURLs([imageURL], options: nil)
            let filename = result.firstObject?.filename ?? ""
           self.filename = filename!
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    // MARK: - UITextFieldDelegate Methods
    
    
    func textFieldDidBeginEditing(textField: UITextField) {    //delegate method
        if(textField == self.emailTxt){
            self.viewUnderEmail.backgroundColor = UIColor(netHex:0xD0842D)
        }else{
            self.viewUnderEmail.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.6)
        }
        if(textField == self.firstNameTxt){
            self.viewUnderFirstname.backgroundColor = UIColor(netHex:0xD0842D)
        }else{
            self.viewUnderFirstname.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.6)
        }
        if(textField == self.lastNameTxt){
            self.viewUnderLastname.backgroundColor = UIColor(netHex:0xD0842D)
        }else{
            self.viewUnderLastname.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.6)
        }
        if(textField == self.passwordTxt){
            self.viewUnderPassword.backgroundColor = UIColor(netHex:0xD0842D)
        }else{
            self.viewUnderPassword.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.6)
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if(textField == self.emailTxt){
            self.viewUnderEmail.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.6)
        }
        if(textField == self.firstNameTxt){
            self.viewUnderFirstname.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.6)
        }
        if(textField==self.lastNameTxt){
            self.viewUnderLastname.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.6)
        }
        if(textField == self.passwordTxt){
            self.viewUnderPassword.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.6)
        }
    }
    //MARK: - Signup
    
    @IBAction func submitButtonAction(sender: AnyObject) {
        var checkAll = true
        
        if(self.firstNameTxt.text == ""){
            checkAll = false
            view.makeToast(message:"First Name Is required", duration: 2, position: HRToastPositionDefault)
            
        }
        if(self.lastNameTxt.text == "" && checkAll != false)
        {
            checkAll = false
            view.makeToast(message:"Last Name Is required", duration: 2, position: HRToastPositionDefault)
            
        }
        if(self.emailTxt.text == "" && checkAll != false)
        {
            checkAll = false
            view.makeToast(message:"Email Is required", duration: 2, position: HRToastPositionDefault)
        }
        if(!self.isValidEmail(self.emailTxt.text!) && checkAll != false){
            checkAll = false
            view.makeToast(message:"Invalid email", duration: 2, position: HRToastPositionDefault)
        }
        if(self.passwordTxt.text == "" && checkAll != false){
            checkAll = false
            view.makeToast(message:"Password is required", duration: 2, position: HRToastPositionDefault)
        }
        if(self.selectedIdentityIndex == 0  && checkAll != false){
            checkAll = false
            view.makeToast(message:"Identity Type Required", duration: 2, position: HRToastPositionDefault)
        }
        if(UIImageJPEGRepresentation(self.image!,0.8) == nil && checkAll != false){
            checkAll = false
            view.makeToast(message:"Identity Doc Required", duration: 2, position: HRToastPositionDefault)
        }
        
        if(checkAll != false){
            let fileUploader = FileUploader()
            fileUploader.addFileData( UIImageJPEGRepresentation(self.image!,0.8)!, withName: "documentIdentity", withMimeType: "image/jpeg", withFileName: self.filename )
            
            
            let request = NSMutableURLRequest( URL: NSURL(string: "\(baseUrl)fileupload/upload/document-identity" )! )
            request.HTTPMethod = "POST"
            fileUploader.uploadFile(request: request)!
                .progress { [weak self] bytesWritten, totalBytesWritten, totalBytesExpectedToWrite in
                    //  To update your ui, dispatch to the main queue.
                    dispatch_async(dispatch_get_main_queue()) {
                        print("Total bytes written on main queue: \(totalBytesWritten)....\(totalBytesExpectedToWrite)")
                    }
                }
                .responseJSON { [weak self] response in
                    switch response.result {
                    case .Success(let data):
                        print(data)
                        let idenDocRes: IdentityDocumentResponse = Mapper<IdentityDocumentResponse>().map(data)!
                       
                        if(idenDocRes.responseStat.status != false){
                            
                            let firstname =  self?.firstNameTxt.text
                            let lastName = self?.lastNameTxt.text
                            let email = self?.emailTxt.text
                            let passWord = self?.passwordTxt.text
                            let identityType = self?.selectedIdentityIndex
                            
                            
                            
                            let paremeters : [String :AnyObject] =
                                ["firstName":firstname!,"lastName":lastName! ,"email" :email! ,"password" :passWord!,"identityTypeId" : identityType!,"identityDocToken" : idenDocRes.responseData]
                            print(paremeters)
                            //
                            Alamofire.request(.POST, NSURL(string: "\(self!.baseUrl)api/signup/user" )!, parameters: paremeters)
                                .validate(contentType: ["application/json"])
                                .responseJSON { response in
                                    switch response.result {
                                    case .Success(let data):
                                       let signUpRes: SignUpResponse = Mapper<SignUpResponse>().map(data)!
                                        print(signUpRes.responseStat.status)
                                       if(signUpRes.responseStat.status != false){
                                        self?.singupMsg = "Signup Successful"
                                        self?.backToLogin()
                                        
                                       }else{
                                        print(signUpRes.responseStat.requestErrors![0].msg)
                                        
                                         self!.view.makeToast(message:signUpRes.responseStat.requestErrors![0].msg, duration: 2, position: HRToastPositionDefault)
                                        
                                        }
                                        
                                    case .Failure(let error):
                                        print(error)
                                    }
                            }
                            
                            
                        }else{
                            print(idenDocRes.responseStat)
                            
                        }
                        
                    case .Failure(let error):
                        print("Request failed with error: \(error)")
                    }
            }
        }
    }
    
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "backToLogin"){
            let contrller : LoginViewController = segue.destinationViewController as! LoginViewController
            contrller.signUpmsg = self.singupMsg
        }
     }
    
    
}
