//
//  ViewController.swift
//  RentGuru
//
//  Created by Workspace Infotech on 7/29/16.
//  Copyright © 2016 Workspace Infotech. All rights reserved.
//

import UIKit


import Alamofire
import ObjectMapper


class LoginViewController: UIViewController ,UITextFieldDelegate{
    @IBOutlet var emailText: UITextField!
    @IBOutlet var passwordText: UITextField!
    @IBOutlet var viewUnderEmail: UIView!
    @IBOutlet var viewUnderPassword: UIView!
    var signUpmsg  = ""
    let defaults = NSUserDefaults.standardUserDefaults()
    var baseUrl : String = ""
    var accessToken : String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        emailText.delegate = self
        passwordText.delegate = self
        baseUrl = defaults.stringForKey("baseUrl")!
        accessToken = defaults.stringForKey("accesstoken")!
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        self.viewUnderEmail.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.6)
        self.viewUnderPassword.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.6)
        self.emailText.attributedPlaceholder = NSAttributedString(string: "Email", attributes:[NSForegroundColorAttributeName : UIColor.lightGrayColor()])
        self.passwordText.attributedPlaceholder = NSAttributedString(string: "Password", attributes:[NSForegroundColorAttributeName : UIColor.lightGrayColor()])
        
        
        if(self.signUpmsg != ""){
            view.makeToast(message:self.signUpmsg, duration: 2, position: HRToastPositionDefault)
        }
        
        self.loginWithAccessToken()
    }
    
    @IBAction func signInAction(sender: AnyObject) {
        var check = true
        if(self.emailText == ""){
            check = false
            view.makeToast(message:"Email is required", duration: 2, position: HRToastPositionDefault)
        }
        if(self.passwordText == "" && check != false){
            check = false
            view.makeToast(message:"Password is required", duration: 2, position: HRToastPositionDefault)
        }
        if(!self.isValidEmail(self.emailText.text!) && check != false){
            check = false
            view.makeToast(message:"Invalid email", duration: 2, position: HRToastPositionDefault)
        }
        if(check != false){
            self.view.makeToastActivity()
            let paremeters: [String :AnyObject] =
                [ "email" : emailText.text!, "password"  : passwordText.text!]
            Alamofire.request(.POST, NSURL(string: "\(baseUrl)api/signin/by-email-password" )!, parameters: paremeters)
                .validate(contentType: ["application/json"])
                .responseJSON { response in
                    self.view.hideToastActivity()
                    switch response.result {
                    case .Success(let data):
                        print(data)
                        let loginRes: LoginResponse = Mapper<LoginResponse>().map(data)!
                        if(loginRes.responseStat.status != false){
                            self.defaults.setObject(loginRes.responseData!.accesstoken, forKey: "accesstoken")
                            
                            
                            
                            self.performSegueWithIdentifier("logInsuccess", sender: nil)
                        }else{
                            self.view.makeToast(message:loginRes.responseStat.msg, duration: 2, position: HRToastPositionDefault)
                        }
                    case .Failure(let error):
                        print(error)
                    }
            }
            
            
        }
        
    }
    
    func loginWithAccessToken()  {
        if(accessToken != "abc"){
            Alamofire.request(.POST, NSURL(string: "\(baseUrl)api/signin/by-accesstoken" )!, parameters: ["accessToken": accessToken])
                .validate(contentType: ["application/json"])
                .responseJSON { response in
                    switch response.result {
                    case .Success(let data):
                        let res: LoginResponse = Mapper<LoginResponse>().map(data)!
                        if((res.responseStat.status) != false){
                            self.performSegueWithIdentifier("logInsuccess", sender: nil)
                        }
                        
                    case .Failure(let error):
                        print(error)
                        
                        
                    }
                    
            }
            
        }
        
    }
    
    func isValidEmail(testStr:String) -> Bool {
        print("validate emilId: \(testStr)")
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluateWithObject(testStr)
        return result
    }
    
    
    /*MARK - TextviewDelegate*/
    //begin//
    
    func textFieldDidBeginEditing(textField: UITextField) {    //delegate method
        if(textField == self.emailText){
            self.viewUnderEmail.backgroundColor = UIColor(netHex:0x996600)
        }else{
            self.viewUnderEmail.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.6)
        }
        if(textField == self.passwordText){
            self.viewUnderPassword.backgroundColor = UIColor(netHex:0x996600)
        }else{
            self.viewUnderPassword.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.6)
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if(textField == self.emailText){
            self.viewUnderEmail.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.6)
        }
        if(textField == self.passwordText){
            self.viewUnderPassword.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.6)
        }
    }
    
    
    //end//
}

