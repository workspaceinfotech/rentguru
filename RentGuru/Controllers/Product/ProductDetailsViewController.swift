//
//  ProductDetailsViewController.swift
//  RentGuru
//
//  Created by Workspace Infotech on 8/9/16.
//  Copyright © 2016 Workspace Infotech. All rights reserved.
//

import UIKit
import Kingfisher
import Alamofire
import ObjectMapper
import Foundation
class ProductDetailsViewController: UIViewController,UICollectionViewDelegateFlowLayout, UICollectionViewDataSource ,RatingViewDelegate {
    
    @IBOutlet var ratingView: RatingView!
    @IBOutlet var baseScroll: UIScrollView!
    @IBOutlet var likeImage: UIImageView!
    @IBOutlet var productName: UILabel!
    @IBOutlet var productProfileImageView: UIImageView!
    @IBOutlet var otherImageCollection: UICollectionView!
    @IBOutlet var rentFee: UILabel!
    @IBOutlet var available: UILabel!
    @IBOutlet var otherImageHeight: NSLayoutConstraint!
    @IBOutlet var categories: UILabel!
    @IBOutlet var descriptionText: UITextView!
    var fromController :String = ""
    var product :RentalProduct!
    var defaults = NSUserDefaults.standardUserDefaults()
    var baseUrl : String = ""
    var imageUrls :[String] = []
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        baseUrl = defaults.stringForKey("baseUrl")!
        
        self.otherImageCollection.delegate = self
        self.otherImageCollection.dataSource = self
        
        var scalingTransform : CGAffineTransform!
        scalingTransform = CGAffineTransformMakeScale(1, -1);
        otherImageCollection.transform = scalingTransform
        
        //self.ratingView.editable = true;
        self.ratingView.delegate = self;
        
       
        self.likeImage.userInteractionEnabled = true
        let likeGesture =  UITapGestureRecognizer(target: self, action:#selector(ProductDetailsViewController.likeProductAction) )
        self.likeImage.addGestureRecognizer(likeGesture)
      
       
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController!.navigationBar.barTintColor = UIColor(netHex:0x2D2D2D)
        self.navigationItem.title =  product.name.uppercaseString
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(netHex:0xD0842D)]
               self.productName.text = product.name.uppercaseString
        self.rentFee.text = "$\(product.rentFee) / \(product.rentType.name)"
        self.descriptionText.text = product.description
        
        if let url  = NSURL(string: "\(baseUrl)/images/\(product.profileImage.original.path)"),
            imageData = NSData(contentsOfURL: url)
        {
            productProfileImageView.image = UIImage(data: imageData)
        }
        var cateString : String = ""
        
        for i in 0 ..< self.product.productCategories!.count{
            if(i == 0){
                cateString = "\(self.product.productCategories![i].category.name)"
            }else {
                cateString = "\(cateString), \(self.product.productCategories![i].category.name)"
            }
        }
        
        self.categories.text = cateString
        
        print(self.product.otherImages)
        if(self.product.otherImages?.isEmpty) != false || self.product.otherImages?.count == 0{
            self.otherImageHeight.constant = 0.0
            self.otherImageCollection.setNeedsUpdateConstraints()
        }else{
            imageUrls.append(product.profileImage.original.path)
            
            for i in 0..<product.otherImages!.count{
                let dataImage : Picture  = product.otherImages![i]
                imageUrls.append(dataImage.original.path)
            }
        }
        
        let timeInter1 :NSTimeInterval = product.availableFrom as NSTimeInterval
        let date1 = NSDate(timeIntervalSince1970: timeInter1/1000)
        let timeInter2 :NSTimeInterval = product.availableTill as NSTimeInterval
        let date2 = NSDate(timeIntervalSince1970: timeInter2/1000)
        
        let dayTimePeriodFormatter = NSDateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMM dd YYYY"
        
        
        
        let dateString1 = dayTimePeriodFormatter.stringFromDate(date1)
        let dateString2 = dayTimePeriodFormatter.stringFromDate(date2)
        
        self.available.text = "\(dateString1) to \(dateString2)"
        self.ratingView.rating = self.product.averageRating
        //        if(product.isLiked == true){
        //          self.likeImage.image = UIImage(named: "fev-full")
        //        }
    }
    
    override func didReceiveMemoryWarning() {
        
    }
    
    @IBAction func rentNowAction(sender: AnyObject) {
        self.performSegueWithIdentifier("rentNow", sender: nil)
    }

    @IBAction func navigateBackAction(sender: AnyObject) {
       // if(self.fromController == "Home"){
            self.performSegueWithIdentifier("Back", sender: nil)
       // }
    }
    
    override func viewDidLayoutSubviews() {
        self.baseScroll.contentSize = CGSizeMake(
            self.view.frame.size.width,
            self.view.frame.size.height + 550
        );
        
    }
    override func viewWillDisappear(animated: Bool)
    {

            super.viewWillDisappear(animated)
            self.navigationController?.navigationBarHidden = true
//
    }
    
    
    
    func likeProductAction() {
        print(self.product.id)
    }
    // MARK:- Collection view Delegate
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //  return   self.product.otherImages!.count
        return self.imageUrls.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! ImageCollectionCell
        
        // Configure the cell
        // let dataImage : Picture  = product.otherImages![indexPath.row]
        
        cell.imageContainer.kf_setImageWithURL(NSURL(string: "\(baseUrl)/images/\(imageUrls[indexPath.row])")!,
                                               placeholderImage:UIImage(named: "placeholder.gif"),
                                               optionsInfo: nil,
                                               progressBlock: { (receivedSize, totalSize) -> () in
                                                //  print("Download Progress: \(receivedSize)/\(totalSize)")
            },
                                               completionHandler: { (image, error, cacheType, imageURL) -> () in
                                                // print("Downloaded and set!")
            }
        )
        
        var scalingTransform : CGAffineTransform!
        scalingTransform = CGAffineTransformMakeScale(1, -1);
        cell.transform = scalingTransform
        
        return cell
        
        
        
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! ImageCollectionCell
        self.productProfileImageView.image = cell.imageContainer.image
        
    }
    
    
    
    //MARK:- RatingViewDelegate
    
    func ratingView(ratingView: RatingView, didChangeRating newRating: Float) {
        print(newRating)
        
        self.view.makeToastActivity()
        Alamofire.request(.GET, NSURL(string: "\(baseUrl)api/auth/product/rate-product/\(self.product.id)/\(newRating)" )!, parameters: [:])
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                self.view.hideToastActivity()
                switch response.result {
                case .Success(let data):
                    let ratingRes: RatingResponse = Mapper<RatingResponse>().map(data)!
                    if(ratingRes.responseStat.status != false){
                        self.view.makeToast(message:"Successfully rated", duration: 2, position: HRToastPositionDefault)
                        
                    }else{
                        self.ratingView.rating = self.product.averageRating
                        self.view.makeToast(message:ratingRes.responseStat.msg, duration: 2, position: HRToastPositionDefault)
                    }
                    
                    
                    
                    
                case .Failure(let error):
                    print(error)
                }
        }
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "rentNow"){
            let navVC = segue.destinationViewController as! UINavigationController
            let rentVC = navVC.viewControllers.first as! RentRequestViewController
          
            rentVC.product = self.product
        }
    }
    
    
}
