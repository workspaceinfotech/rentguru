//
//  RentRequestViewController.swift
//  RentGuru
//
//  Created by Workspace Infotech on 8/18/16.
//  Copyright © 2016 Workspace Infotech. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
class RentRequestViewController: UIViewController ,EPCalendarPickerDelegate {
    
    @IBOutlet var fromDateView: UIView!
    @IBOutlet var fromDateSelector: UIImageView!
    @IBOutlet var toDateSelector: UIImageView!
    @IBOutlet var toDateView: UIView!
    @IBOutlet var startDate: UITextField!
    @IBOutlet var endDate: UITextField!
    @IBOutlet var remark: UITextView!
    var product : RentalProduct!
    var fromController : String!
    let fromCalendarPicker = EPCalendarPicker(startYear: 2016, endYear: 2017, multiSelection: true, selectedDates: [])
    let toCalendarPicker = EPCalendarPicker(startYear: 2016, endYear: 2017, multiSelection: true, selectedDates: [])
    var didTouchedFromDate = false
    var didTouchedToDate = false
    let defaults = NSUserDefaults.standardUserDefaults()
    var baseUrl : String = ""
    var presentWindow : UIWindow?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        baseUrl = defaults.stringForKey("baseUrl")!
        presentWindow = UIApplication.sharedApplication().keyWindow
        self.fromDateSelector.userInteractionEnabled = true
        self.fromDateView.userInteractionEnabled = true
        let fromDateGexture = UITapGestureRecognizer(target: self, action:#selector(RentRequestViewController.selectFromDateAction) )
        self.fromDateSelector.addGestureRecognizer(fromDateGexture)
        self.fromDateView.addGestureRecognizer(fromDateGexture)
        
        self.toDateSelector.userInteractionEnabled = true
        self.toDateView.userInteractionEnabled = true
        let toDateGesture =  UITapGestureRecognizer(target: self, action:#selector(RentRequestViewController.selectToDateAction) )
        self.toDateSelector.addGestureRecognizer(toDateGesture)
        self.toDateView.addGestureRecognizer(toDateGesture)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.navigationBar.barTintColor = UIColor(netHex:0x2D2D2D)
        self.navigationItem.title =  "Request To Rent"
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(netHex:0xD0842D)]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backToDetailsAction(sender: AnyObject) {
        self.performSegueWithIdentifier("backToDetails", sender: nil)
    }
    
    
    func selectFromDateAction()
    {
        
        self.didTouchedFromDate = true;
        fromCalendarPicker.calendarDelegate = self
        fromCalendarPicker.startDate = NSDate()
        fromCalendarPicker.hightlightsToday = true
        fromCalendarPicker.showsTodaysButton = true
        fromCalendarPicker.hideDaysFromOtherMonth = true
        fromCalendarPicker.tintColor = UIColor.orangeColor()
        fromCalendarPicker.multiSelectEnabled = false
        //        calendarPicker.barTintColor = UIColor.greenColor()
        fromCalendarPicker.dayDisabledTintColor = UIColor.grayColor()
        fromCalendarPicker.title = "Start From "
        
        //        calendarPicker.backgroundImage = UIImage(named: "background_image")
        //        calendarPicker.backgroundColor = UIColor.blueColor()
        
        let navigationController = UINavigationController(rootViewController: fromCalendarPicker)
        self.presentViewController(navigationController, animated: true, completion: nil)
    }
    func selectToDateAction()  {
        self.didTouchedToDate = true
        toCalendarPicker.calendarDelegate = self
        toCalendarPicker.startDate = NSDate()
        toCalendarPicker.hightlightsToday = true
        toCalendarPicker.showsTodaysButton = true
        toCalendarPicker.hideDaysFromOtherMonth = true
        toCalendarPicker.tintColor = UIColor.orangeColor()
        toCalendarPicker.multiSelectEnabled = false
        //        calendarPicker.barTintColor = UIColor.greenColor()
        toCalendarPicker.dayDisabledTintColor = UIColor.grayColor()
        toCalendarPicker.title = "End Date"
        
        //        calendarPicker.backgroundImage = UIImage(named: "background_image")
        //        calendarPicker.backgroundColor = UIColor.blueColor()
        
        let navigationController = UINavigationController(rootViewController: toCalendarPicker)
        self.presentViewController(navigationController, animated: true, completion: nil)
    }
    // MARK: - EPCalendarPicker
    
    
    func epCalendarPicker(_: EPCalendarPicker, didCancel error : NSError) {
        //if(self == self.fromCalendarPicker)
        //  fromDateTxt.text = "User cancelled selection"
        
    }
    func epCalendarPicker(_: EPCalendarPicker, didSelectDate date : NSDate) {
        
        let formatter = NSDateFormatter()
        formatter.dateFormat = "dd-MM-yyy"
        let str = formatter.stringFromDate(date)
        
        if(self.didTouchedFromDate){
            self.didTouchedFromDate = false
            self.startDate.text = str//"User selected date: \n\(date)"
        }
        if(self.didTouchedToDate){
            self.didTouchedToDate = false
            self.endDate.text = str
        }
        
        
        
    }
    @IBAction func submitRequest(sender: AnyObject) {
        var check : Bool = true
        if(startDate.text == ""){
            check = false
            self.view.makeToast(message:"Start Date Required", duration: 2, position: HRToastPositionCenter)
        }
        if(endDate.text == "" && check != false){
            check = false
            self.view.makeToast(message:"Start Date Required", duration: 2, position: HRToastPositionCenter)
        }
        if(check != false){
            self.postRentRequest()
        }
    }
    
    //MARK:- API Access
    
    
    func postRentRequest()  {
        self.presentWindow!.makeToastActivity()
        let paremeters : [String :AnyObject] =
            ["startDate" : self.startDate.text!,"endsDate" : self.endDate.text!,"remark":self.remark.text!]
        print(paremeters)
        Alamofire.request(.POST, NSURL(string: "\(baseUrl)api/auth/rent/make-request/\(self.product.id)" )!, parameters: paremeters)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                print(response)
                self.presentWindow!.hideToastActivity()
                switch response.result {
                case .Success(let data):
                    let proRes: RentRequestResponse = Mapper<RentRequestResponse>().map(data)!
                    
                    if(proRes.responseStat.status != false){
                        self.view.makeToast(message:"Request Successful", duration: 2, position: HRToastPositionCenter)
                        self.startDate.text! = ""
                        self.endDate.text! = ""
                        self.remark.text! = ""
                    }else{
                        if(proRes.responseStat.requestErrors?.count > 0){
                            
                            self.view.makeToast(message:proRes.responseStat.requestErrors![0].msg, duration: 2, position: HRToastPositionCenter)
                        }else{
                            self.view.makeToast(message:proRes.responseStat.msg, duration: 2, position: HRToastPositionCenter)

                        }
                    }
                    
                    
                case .Failure(let error):
                    print(error)
                }
        }
    }
    
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let navVC = segue.destinationViewController as! UINavigationController
        let detailsVC = navVC.viewControllers.first as! ProductDetailsViewController
        detailsVC.product = self.product
        
    }
    
}
