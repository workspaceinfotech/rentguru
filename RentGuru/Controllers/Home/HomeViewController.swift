//
//  HomeViewController.swift
//  RentGuru
//
//  Created by Workspace Infotech on 8/1/16.
//  Copyright © 2016 Workspace Infotech. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import Kingfisher
class HomeViewController: UIViewController,UICollectionViewDelegateFlowLayout, UICollectionViewDataSource ,CollectionViewWaterfallLayoutDelegate,UIScrollViewDelegate{
    @IBOutlet var productScrollHeight: NSLayoutConstraint!
    @IBOutlet var featuredProductCollection: UICollectionView!
    @IBOutlet var offerCollection: UICollectionView!
    @IBOutlet var productScrollBottom: NSLayoutConstraint!
    @IBOutlet var baseScroll: UIScrollView!
    var defaults = NSUserDefaults.standardUserDefaults()
    var baseUrl : String = ""
    var allProducts:[RentalProduct] = []
    var offset : Int = 0
    var isData : Bool = true
    var selectedProdIndex : Int = 0
    lazy var cellSizes: [CGSize] = {
        var _cellSizes = [CGSize]()
        
        for _ in 0...100 {
            let random = Int(arc4random_uniform((UInt32(100))))
            
            _cellSizes.append(CGSize(width: 155, height: 150 + random))
        }
        
        return _cellSizes
    }()
    private  var lastContentOffset: CGFloat = 0
    var presentWindow : UIWindow?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.featuredProductCollection.delegate = self
        self.featuredProductCollection.dataSource = self
        self.offerCollection.delegate = self
        self.offerCollection.dataSource = self
        self.baseScroll.delegate = self
        baseUrl = defaults.stringForKey("baseUrl")!
        presentWindow = UIApplication.sharedApplication().keyWindow
        //staggered View
        
        let layout = CollectionViewWaterfallLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        layout.headerInset = UIEdgeInsetsMake(20, 0, 0, 0)
        layout.headerHeight = 50
        layout.footerHeight = 20
        layout.minimumColumnSpacing = 10
        layout.minimumInteritemSpacing = 10
        
        self.offerCollection.collectionViewLayout = layout
        self.offerCollection.allowsSelection = true
        
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.offset = 0
        self.isData = true
        self.allProducts.removeAll()
        self.getProducts()
    }
    override func viewDidLayoutSubviews() {
        self.baseScroll.contentSize = CGSizeMake(
            self.view.frame.size.width,
            self.view.frame.size.height + 206
        );
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func perfromNext(sender :UITapGestureRecognizer)  {
        self.view.makeToastActivity()
        let tapLocation = sender.locationInView(self.offerCollection)
        let indexPath : NSIndexPath = self.offerCollection.indexPathForItemAtPoint(tapLocation)!
        
        if let cell = self.offerCollection.cellForItemAtIndexPath(indexPath)
        {
            self.selectedProdIndex = cell.tag
            
        }
        dispatch_async(dispatch_get_main_queue(),{
            self.performSegueWithIdentifier("showDetails", sender: nil)
        })
        
        
    }
    // MARK: - collectionview delegate
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == self.offerCollection){
            return self.allProducts.count
        }
        return 10;
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if(collectionView == self.offerCollection ){
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("offerCell", forIndexPath: indexPath) as!OfferStaggeredCell
            let data : RentalProduct = allProducts [indexPath.row]
            
            cell.productImageView.kf_setImageWithURL(NSURL(string: "\(baseUrl)/images/\(data.profileImage.original.path)")!,
                                                     placeholderImage: UIImage(named: "placeholder.gif"),
                                                     optionsInfo: nil,
                                                     progressBlock: { (receivedSize, totalSize) -> () in
                                                        //  print("Download Progress: \(receivedSize)/\(totalSize)")
                },
                                                     completionHandler: { (image, error, cacheType, imageURL) -> () in
                                                        // print("Downloaded and set!")
                }
            )
            
            // cell.productImageView.image = UIImage(named:"dis1.png")
            cell.productName.text = data.name
            cell.offerLabel.text = "\(data.rentFee)"
            
            cell.tag = indexPath.row
            cell.userInteractionEnabled = true
            let selectGesture = UITapGestureRecognizer(target: self, action:#selector(HomeViewController.perfromNext) )
            //selectGesture.numberOfTapsRequired = 2
            cell.addGestureRecognizer(selectGesture)
            
            return cell
            
            
        }
        else{
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("featuredCell", forIndexPath: indexPath) as!FeaturedProductsCell
            
            // Configure the cell
            cell.productImageView.image = UIImage(named:"thumb1.png")
            cell.productName.text = "Delux Bed"
            cell.productPrice.text = "$300/m"
            
            return cell
        }
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        
    }
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        
        if(scrollView == self.offerCollection){
            let offsetY = self.offerCollection.contentOffset.y
            let contentHeight = self.offerCollection.contentSize.height
            if offsetY > contentHeight - self.offerCollection.frame.size.height {
                print("this is end, see you in console")
                self.getProducts()
            }
        }else if(scrollView == self.baseScroll){
            
            print("baseScrolled ")
            
            if (self.lastContentOffset > scrollView.contentOffset.y) {
                UIView.animateWithDuration(0.5) {
                    self.productScrollHeight.constant =  self.productScrollHeight.constant - 412
                    self.productScrollBottom.constant =  self.productScrollBottom.constant + 206
                    self.view.layoutIfNeeded()
                }
                
                
            }
            else if (self.lastContentOffset < scrollView.contentOffset.y) {
                // print("Moved Down \(self.lastContentOffset)")
                
                UIView.animateWithDuration(0.5) {
                    self.productScrollHeight.constant =  self.productScrollHeight.constant + 412
                    self.productScrollBottom.constant =  self.productScrollBottom.constant - 206
                    self.view.layoutIfNeeded()
                }
                
            }
            
            // update the new position acquired
            self.lastContentOffset = scrollView.contentOffset.y
            self.getProducts()
        }
    }
    // MARK: - WaterfallLayoutDelegate
    
    func collectionView(collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if(collectionView == self.offerCollection){
            return cellSizes[indexPath.item]
        }else{
            return CGSize(width: 122, height:135)
        }
        
    }
    
    //Mark : - Api Call
    
    func getProducts() {
        if(self.isData != false){
            presentWindow!.makeToastActivity()
            let  paremeters :[String:AnyObject] = ["limit" : 6 , "offset" : self.offset ]
            Alamofire.request(.GET, NSURL(string: "\(baseUrl)api/product/get-product" )!, parameters: paremeters)
                .validate(contentType: ["application/json"])
                .responseJSON { response in
                    switch response.result {
                    case .Success(let data):
                        //  print(data)
                        let proRes: ProductResponse = Mapper<ProductResponse>().map(data)!
                        //  print(proRes.responseStat.status)
                        
                        if(proRes.responseStat.status != false){
                            //  print(proRes.responseData!)
                            for i in 0 ..< proRes.responseData!.count {
                                let product : RentalProduct = proRes.responseData![i]
                                self.allProducts.append(product)
                            }
                            self.offset += 1
                            self.offerCollection.reloadData()
                            
                        }else{
                            self.isData = false
                            self.view.makeToast(message:"No more data", duration: 2, position: HRToastPositionCenter)
                        }
                        self.presentWindow!.hideToastActivity()
                    case .Failure(let error):
                        print(error)
                        
                    }
            }
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "showDetails"){
            
            let navVC = segue.destinationViewController as! UINavigationController
            let detailsVC = navVC.viewControllers.first as! ProductDetailsViewController
            detailsVC.product = self.allProducts[self.selectedProdIndex]
            detailsVC.fromController = "Home"
        }
    }
    
}