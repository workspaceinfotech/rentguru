//
//  RequestDetailsViewController.swift
//  RentGuru
//
//  Created by Workspace Infotech on 8/27/16.
//  Copyright © 2016 Workspace Infotech. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper






class RequestDetailsViewController: UIViewController ,UICollectionViewDelegateFlowLayout, UICollectionViewDataSource  {
    
    let defaults = NSUserDefaults.standardUserDefaults()
    var baseUrl : String = ""
    @IBOutlet var userProfilePicImageView: UIImageView!
    
    @IBOutlet var userAddressLbl: UILabel!
    @IBOutlet var userEmailLbl: UILabel!
    @IBOutlet var userPhoneNoLbl: UILabel!
    @IBOutlet var userNameLbl: UILabel!
    @IBOutlet var rentTypeLbl: UILabel!
    @IBOutlet var rentFeeLbl: UILabel!
    @IBOutlet var productOverviewtxt: UITextView!
    @IBOutlet var productAvailableFromLbl: UILabel!
    @IBOutlet var availableTillLbl: UILabel!
    
    @IBOutlet var productNameLbl: UILabel!
    @IBOutlet var productProfileImageView: UIImageView!
    @IBOutlet var totalAmount: UILabel!
    @IBOutlet var totalDaysLbl: UILabel!
    @IBOutlet var orderEndDate: UILabel!
    @IBOutlet var orderStartDateLbl: UILabel!
    
    @IBOutlet var negetiveBtn: UIButton!
    @IBOutlet var positiveBtn: UIButton!
    @IBOutlet var remarkLbl: UITextView!
    @IBOutlet var otherImagesCollection: UICollectionView!
    
    @IBOutlet var remarkHeight: NSLayoutConstraint!
    @IBOutlet var productOverviewHeight: NSLayoutConstraint!
    @IBOutlet var otherImageHeight: NSLayoutConstraint!
    var isRequestedToMe : Bool!
    var rentRequest : RentRequest!
    var  imageUrls : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        baseUrl = defaults.stringForKey("baseUrl")!
        // Do any additional setup after loading the view.
        self.otherImagesCollection.delegate = self
        self.otherImagesCollection.dataSource = self
        
        let fixedWidth = remarkLbl.frame.size.width
        remarkLbl.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.max))
        let newSize = remarkLbl.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.max))
        var newFrame = remarkLbl.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        remarkLbl.frame = newFrame;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
       self.navigationItem.title = "Request Details"
        if(self.rentRequest.rentalProduct.otherImages?.isEmpty) != false || self.rentRequest.rentalProduct.otherImages?.count == 0{
            self.otherImageHeight.constant = 0.0
            self.otherImagesCollection.setNeedsUpdateConstraints()
        }else{
            imageUrls.append(rentRequest.rentalProduct.profileImage.original.path)
            
            for i in 0..<rentRequest.rentalProduct.otherImages!.count{
                let dataImage : Picture  = rentRequest.rentalProduct.otherImages![i]
                imageUrls.append(dataImage.original.path)
            }
        }

        
       
        if(self.isRequestedToMe == true){
            self.userNameLbl.text = "\(rentRequest.requestedBy.userInf.firstName) \(rentRequest.requestedBy.userInf.lastName)"
            self.userEmailLbl.text = "\(rentRequest.requestedBy.email)"
            self.userAddressLbl.text =  "\(rentRequest.requestedBy.userInf.userAddress.address) \(rentRequest.requestedBy.userInf.userAddress.city) \(rentRequest.requestedBy.userInf.userAddress!.zip)"
            print(rentRequest.requestedBy.userInf)
//            self.userProfilePicImageView.kf_setImageWithURL(NSURL(string: "\(baseUrl)/images/\(rentRequest.requestedBy.userInf.profilePicture!.original.path)")!,
//                                                             placeholderImage: nil,
//                                                             optionsInfo: nil,
//                                                             progressBlock: { (receivedSize, totalSize) -> () in
//                                                                //  print("Download Progress: \(receivedSize)/\(totalSize)")
//                },
//                                                             completionHandler: { (image, error, cacheType, imageURL) -> () in
//                                                                // print("Downloaded and set!")
//                }
//            )
          
        }else{
            self.userNameLbl.text = "\(rentRequest.rentalProduct.owner.userInf.firstName) \(rentRequest.rentalProduct.owner.userInf.lastName)"
            self.userEmailLbl.text = "\(rentRequest.requestedBy.email)"
            self.userAddressLbl.text =  "\(rentRequest.rentalProduct.productLocation!.formattedAddress!) \(rentRequest.rentalProduct.productLocation!.city!) \(rentRequest.rentalProduct.productLocation!.zip!)"
            print(rentRequest.requestedBy.userInf)
//            self.userProfilePicImageView.kf_setImageWithURL(NSURL(string: "\(baseUrl)/images/\(rentRequest.rentalProduct.owner.userInf.profilePicture!.original.path)")!,
//                                                            placeholderImage: nil,
//                                                            optionsInfo: nil,
//                                                            progressBlock: { (receivedSize, totalSize) -> () in
//                                                                //  print("Download Progress: \(receivedSize)/\(totalSize)")
//                },
//                                                            completionHandler: { (image, error, cacheType, imageURL) -> () in
//                                                                // print("Downloaded and set!")
//                }
//            )
        
        }
        self.orderStartDateLbl.text = rentRequest.startDate
        self.orderEndDate.text = rentRequest.endDate
        self.rentTypeLbl.text = "Rent/\(rentRequest.rentalProduct.rentType.name)"
        self.rentFeeLbl.text = "\(rentRequest.rentalProduct.rentFee)"
        
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let difference  = NSCalendar.currentCalendar().components(.Day, fromDate: dateFormatter.dateFromString(rentRequest.startDate)!, toDate: dateFormatter.dateFromString( rentRequest.endDate )!, options: []).day
        self.totalDaysLbl.text = "\(difference + 1) Days"
        self.totalAmount.text = "\(Double(difference + 1)*rentRequest.rentalProduct.rentFee)"
       
        
        
        self.remarkLbl.text = rentRequest.remark
        
        self.productProfileImageView.kf_setImageWithURL(NSURL(string: "\(baseUrl)/images/\(rentRequest.rentalProduct.profileImage.original.path)")!,
                                                                               placeholderImage: nil,
                                                                               optionsInfo: nil,
                                                                               progressBlock: { (receivedSize, totalSize) -> () in
                                                                                //  print("Download Progress: \(receivedSize)/\(totalSize)")
            },
                                                                               completionHandler: { (image, error, cacheType, imageURL) -> () in
                                                                                // print("Downloaded and set!")
            }
        )
        self.productNameLbl.text =  rentRequest.rentalProduct.name
        self.productOverviewtxt.text = rentRequest.rentalProduct.description
       
        let timeInter1 :NSTimeInterval = rentRequest.rentalProduct.availableFrom as NSTimeInterval
        let date1 = NSDate(timeIntervalSince1970: timeInter1/1000)
        let timeInter2 :NSTimeInterval = rentRequest.rentalProduct.availableTill as NSTimeInterval
        let date2 = NSDate(timeIntervalSince1970: timeInter2/1000)
        
        let dayTimePeriodFormatter = NSDateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMM dd YYYY"
        
        
        
        let dateString1 = dayTimePeriodFormatter.stringFromDate(date1)
        let dateString2 = dayTimePeriodFormatter.stringFromDate(date2)
        
        self.productAvailableFromLbl.text = "\(dateString1)"
        self.availableTillLbl.text  =  dateString2
        

        
        //ButtonControl
        if(isRequestedToMe == true && rentRequest.approve == false && rentRequest.disapprove == false){
            self.positiveBtn.setTitle("Accept", forState: .Normal)
            self.negetiveBtn.setTitle("Decline", forState:.Normal)
            self.positiveBtn.addTarget(self, action: #selector(acceptAction), forControlEvents: .TouchUpInside)
              self.negetiveBtn.addTarget(self, action: #selector(declineAction), forControlEvents: .TouchUpInside)
        }else if(isRequestedToMe == true && rentRequest.approve == true && rentRequest.disapprove == false){
         self.positiveBtn.setTitle("Accepted", forState: .Normal)
            self.negetiveBtn.hidden = true
        }else if(isRequestedToMe == true && rentRequest.approve == false && rentRequest.disapprove == true){
            self.negetiveBtn.setTitle("Declined", forState:.Normal)
            self.positiveBtn.hidden = true
        }else if(isRequestedToMe == false && rentRequest.approve == false && rentRequest.disapprove == false && rentRequest.requestCancel == false){
            self.negetiveBtn.setTitle("Cancel", forState:.Normal)
            self.positiveBtn.hidden = true
            self.negetiveBtn.addTarget(self, action: #selector(cancelAction), forControlEvents: .TouchUpInside)
        }
        else if(isRequestedToMe == false && rentRequest.approve == false && rentRequest.disapprove == false && rentRequest.requestCancel == true){
            self.negetiveBtn.setTitle("Canceled", forState:.Normal)
            self.positiveBtn.hidden = true
            
        }

      
    }
    
    // MARK:- Collection view Delegate
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //  return   self.product.otherImages!.count
        return self.imageUrls.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! ImageCollectionCell
        
        // Configure the cell
        // let dataImage : Picture  = product.otherImages![indexPath.row]
        
        cell.imageContainer.kf_setImageWithURL(NSURL(string: "\(baseUrl)/images/\(imageUrls[indexPath.row])")!,
                                               placeholderImage:UIImage(named: "placeholder.gif"),
                                               optionsInfo: nil,
                                               progressBlock: { (receivedSize, totalSize) -> () in
                                                //  print("Download Progress: \(receivedSize)/\(totalSize)")
            },
                                               completionHandler: { (image, error, cacheType, imageURL) -> () in
                                                // print("Downloaded and set!")
            }
        )
        
        var scalingTransform : CGAffineTransform!
        scalingTransform = CGAffineTransformMakeScale(1, -1);
        cell.transform = scalingTransform
        
        return cell
        
        
        
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! ImageCollectionCell
        self.productProfileImageView.image = cell.imageContainer.image
        
    }
 //MARK:- APIAccess
    
    func acceptAction(){
        let requestId = self.rentRequest.id
        
        Alamofire.request(.GET, NSURL(string: "\(baseUrl)api/auth/rent/approve-request/\(requestId)" )!, parameters: [:])
            .validate(contentType: ["application/json"])
            .responseJSON { response in
            print(response)
                switch response.result {
                case .Success(let data):
                    let result : Response = Mapper<Response>().map(data)!
                    if(result.responseStat.status != false){
                        self.view.makeToast(message:"Request Accepted", duration: 2, position: HRToastPositionCenter)
                        self.positiveBtn.setTitle("Accepted", forState: .Normal)
                        self.negetiveBtn.hidden = true
                      self.positiveBtn.removeTarget(self, action: Selector(), forControlEvents: UIControlEvents.AllEvents)
                        
                    }else{
                        self.view.makeToast(message:result.responseStat.requestErrors![0].msg, duration: 2, position: HRToastPositionCenter)
                    }
                case .Failure(let error):
                    print(error)
                }
        }
        
    }
    func declineAction()  {
        
         let requestId = self.rentRequest.id
        
        Alamofire.request(.GET, NSURL(string: "\(baseUrl)api/auth/rent/disapprove-request/\(requestId)" )!, parameters: [:])
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                //print(response)
                switch response.result {
                case .Success(let data):
                    let result : Response = Mapper<Response>().map(data)!
                    if(result.responseStat.status != false){
                        self.view.makeToast(message:"Request Declined", duration: 2, position: HRToastPositionCenter)
                        self.negetiveBtn.setTitle("Declined", forState:.Normal)
                        self.positiveBtn.hidden = true
                       self.negetiveBtn.removeTarget(self, action: Selector(), forControlEvents: UIControlEvents.AllEvents)
                    }else{
                         self.view.makeToast(message:result.responseStat.requestErrors![0].msg, duration: 2, position: HRToastPositionCenter)
                    }
                case .Failure(let error):
                    print(error)
                }
        }
    }
    func cancelAction(){
        let requestId = self.rentRequest.id
        Alamofire.request(.GET, NSURL(string: "\(baseUrl)api/auth/rent/cancel-request/\(requestId)" )!, parameters: [:])
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                print(response)
                switch response.result {
                case .Success(let data):
                    let result : RentRequestActionResponse = Mapper<RentRequestActionResponse>().map(data)!
                    if(result.responseStat.status != false){
                        self.view.makeToast(message:"Request Cenceled", duration: 2, position: HRToastPositionCenter)
                        self.negetiveBtn.setTitle("Canceled", forState:.Normal)
                       self.negetiveBtn.removeTarget(self, action: Selector(), forControlEvents: UIControlEvents.AllEvents)
                        self.positiveBtn.hidden = true
                        
                    }else{
                        self.view.makeToast(message:result.responseStat.msg, duration: 2, position: HRToastPositionCenter)
                    }
                case .Failure(let error):
                    print(error)
                }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
   
    
}
