//
//  MyRequestViewController.swift
//  RentGuru
//
//  Created by Workspace Infotech on 8/19/16.
//  Copyright © 2016 Workspace Infotech. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import Kingfisher

class MyRequestViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    @IBOutlet var myRequestTableView: UITableView!
    @IBOutlet var segmentedView: UISegmentedControl!
    let defaults = NSUserDefaults.standardUserDefaults()
    var baseUrl : String = ""
    var presentWindow : UIWindow?
    var offset : Int = 0
    var requestList : [RentRequest]! = []
    var currentIndex :Int = 0
    var isData : Bool = true
    var selectedRentRequest :RentRequest!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presentWindow = UIApplication.sharedApplication().keyWindow
        baseUrl = defaults.stringForKey("baseUrl")!
        self.myRequestTableView.delegate = self
        self.myRequestTableView.dataSource = self
        self.tabBarController?.tabBar.hidden = true
       
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Requested"
        self.getMyPendingRentRequest()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func segmentedViewChangedAction(sender: AnyObject) {
        self.currentIndex = segmentedView.selectedSegmentIndex
        self.offset = 0
        self.isData = true
        self.requestList.removeAll()
        
        switch segmentedView.selectedSegmentIndex
        {
        case 0:
            self.getMyPendingRentRequest()
        case 1:
            self.getMyApprovedRentRequest()
        case 2:
            self.getMyDisapprovedRentRequest()
        default:
            break;
        }
    }
    
    //MARK :- TableviewDelegate
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.requestList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:MyRequestTableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell") as! MyRequestTableViewCell
        let  data : RentRequest = self.requestList[indexPath.row]
        cell.productImageView.kf_setImageWithURL(NSURL(string: "\(baseUrl)/images/\(data.rentalProduct.profileImage.original.path)")!,
                                                 placeholderImage: nil,
                                                 optionsInfo: nil,
                                                 progressBlock: { (receivedSize, totalSize) -> () in
                                                    //  print("Download Progress: \(receivedSize)/\(totalSize)")
            },
                                                 completionHandler: { (image, error, cacheType, imageURL) -> () in
                                                    // print("Downloaded and set!")
            }
        )
        cell.productName.text = data.rentalProduct.name
        cell.ownerName.text = "\(data.rentalProduct.owner.userInf.firstName) \(data.rentalProduct.owner.userInf.lastName)"
        cell.dateRange.text = "\( data.startDate) to \(data.endDate)"
        return cell
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let  data : RentRequest = self.requestList[indexPath.row]
        
        if(data.requestCancel == false && data.approve == false &&  data.disapprove == false)  {
            let cancel = UITableViewRowAction(style: .Normal, title: "Cancel") { action, index in
                self.cancelAction(data.id)
            }
            cancel.backgroundColor = UIColor(netHex:0xD0842D)
            return [ cancel]
        }else if( data.approve == true && data.disapprove == false ){
            let accept = UITableViewRowAction(style: .Normal, title: "Accepted") { action, index in
                print("favorite button tapped")
            }
            accept.backgroundColor = UIColor(netHex:0xD0842D)
            
            return [accept]
        }else if(data.disapprove == true && data.approve == false ){
            let decline = UITableViewRowAction(style: .Normal, title: "Declined") { action, index in
                print("share button tapped")
            }
            decline.backgroundColor = UIColor.redColor()
            
            return [decline]
        }else{
         return nil
        }
    }
    
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // the cells you would like the actions to appear needs to be editable
        return true
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedRentRequest = requestList[indexPath.row]
        self.performSegueWithIdentifier("requestDetails", sender: nil)
    }
    
    func cancelAction(requestId: Int){
        Alamofire.request(.GET, NSURL(string: "\(baseUrl)api/auth/rent/cancel-request/\(requestId)" )!, parameters: [:])
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                //print(response)
                switch response.result {
                case .Success(let data):
                    let result : Response = Mapper<Response>().map(data)!
                    if(result.responseStat.status != false){
                        self.view.makeToast(message:"Request Cenceled", duration: 2, position: HRToastPositionCenter)
                        self.offset = 0
                        self.isData = true
                        self.requestList.removeAll()
                        switch  self.currentIndex
                        {
                        case 0:
                            self.getMyPendingRentRequest()
                        case 1:
                            self.getMyApprovedRentRequest()
                        case 2:
                            self.getMyDisapprovedRentRequest()
                        default:
                            break;
                        }
                        
                    }else{
                        self.view.makeToast(message:result.responseStat.msg, duration: 2, position: HRToastPositionCenter)
                    }
                case .Failure(let error):
                    print(error)
                }
        }
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        switch  self.currentIndex
        {
        case 0:
            self.getMyPendingRentRequest()
        case 1:
            self.getMyApprovedRentRequest()
        case 2:
            self.getMyDisapprovedRentRequest()
        default:
            break;
        }
    }
    
    //MARK :- API Access
    func getMyPendingRentRequest() {
        if(self.isData == true){
            presentWindow!.makeToastActivity()
            let  paremeters :[String:AnyObject] = ["limit" : 6 , "offset" : self.offset ]
            Alamofire.request(.POST, NSURL(string: "\(baseUrl)api/auth/rent/get-my-pending-rent-request" )!, parameters: paremeters)
                //  .validate(contentType: ["application/json"])
                .responseJSON { response in
                    switch response.result {
                    case .Success(let data):
                        //   print(data)
                        let reqRes : MyProductRentRequestResponse = Mapper<MyProductRentRequestResponse>().map(data)!
                        //  print(reqRes)
                        if(reqRes.responseStat.status != false){
                            for i in 0 ..< reqRes.responseData!.count{
                                self.requestList.append(reqRes.responseData![i])
                                
                            }
                            self.offset += 1
                            self.myRequestTableView.reloadData()
                            
                        }else{
                            self.isData = false
                            if(self.requestList.count == 0){
                                self.myRequestTableView.reloadData()
                            }
                            self.view.makeToast(message:"No more data", duration: 2, position: HRToastPositionCenter)
                            
                        }
                        
                        
                        
                    case .Failure(let error):
                        print(error)
                        
                    }
                    self.presentWindow!.hideToastActivity()
            }
            
        }
        
    }
    func getMyApprovedRentRequest() {
        if(self.isData == true){
            presentWindow!.makeToastActivity()
            let  paremeters :[String:AnyObject] = ["limit" : 6 , "offset" : self.offset ]
            Alamofire.request(.POST, NSURL(string: "\(baseUrl)api/auth/rent/get-my-approved-rent-request" )!, parameters: paremeters)
                //  .validate(contentType: ["application/json"])
                .responseJSON { response in
                    switch response.result {
                    case .Success(let data):
                          print(data)
                        let reqRes : MyProductRentRequestResponse = Mapper<MyProductRentRequestResponse>().map(data)!
                        //  print(reqRes)
                        if(reqRes.responseStat.status != false){
                            for i in 0 ..< reqRes.responseData!.count{
                                self.requestList.append(reqRes.responseData![i])
                                
                            }
                            self.offset += 1
                            self.myRequestTableView.reloadData()
                            
                        }else{
                            self.isData = false
                            if(self.requestList.count == 0){
                                self.myRequestTableView.reloadData()
                            }
                            self.view.makeToast(message:"No more data", duration: 2, position: HRToastPositionCenter)
                            
                        }
                        
                        
                        
                    case .Failure(let error):
                        print(error)
                        
                    }
                    self.presentWindow!.hideToastActivity()
            }
            
        }
        
    }
    func getMyDisapprovedRentRequest() {
        if(self.isData == true){
            presentWindow!.makeToastActivity()
            let  paremeters :[String:AnyObject] = ["limit" : 6 , "offset" : self.offset ]
            Alamofire.request(.POST, NSURL(string: "\(baseUrl)api/auth/rent/get-my-disapproved-rent-request" )!, parameters: paremeters)
                //  .validate(contentType: ["application/json"])
                .responseJSON { response in
                    switch response.result {
                    case .Success(let data):
                        //   print(data)
                        let reqRes : MyProductRentRequestResponse = Mapper<MyProductRentRequestResponse>().map(data)!
                        //  print(reqRes)
                        if(reqRes.responseStat.status != false){
                            for i in 0 ..< reqRes.responseData!.count{
                                self.requestList.append(reqRes.responseData![i])
                                
                            }
                            self.offset += 1
                            self.myRequestTableView.reloadData()
                            
                        }else{
                            self.isData = false
                            if(self.requestList.count == 0){
                                self.myRequestTableView.reloadData()
                            }
                            self.view.makeToast(message:"No more data", duration: 2, position: HRToastPositionCenter)
                        }
                        
                        
                        
                    case .Failure(let error):
                        print(error)
                        
                    }
                    self.presentWindow!.hideToastActivity()
            }
            
        }
        
    }
  
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "requestDetails"){
            self.navigationController!.navigationBar.barTintColor = UIColor(netHex:0x2D2D2D)
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(netHex:0xD0842D)]
            
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.Plain, target:nil, action:nil)
            self.navigationItem.backBarButtonItem!.tintColor = UIColor.whiteColor()
            
            let controller : RequestDetailsViewController = segue.destinationViewController as! RequestDetailsViewController
            controller.isRequestedToMe = false
            controller.rentRequest = self.selectedRentRequest
            
        }
    
     }
    
}
