//
//  MyProductsViewController.swift
//  RentGuru
//
//  Created by Workspace Infotech on 8/23/16.
//  Copyright © 2016 Workspace Infotech. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
class MyProductsViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var myProductTable: UITableView!
    
    let defaults = NSUserDefaults.standardUserDefaults()
    var baseUrl : String = ""
    var presentWindow : UIWindow?
    var offset : Int = 0
    var productList : [MyRentalProduct]! = []
    var isData : Bool = true
 
    override func viewDidLoad() {
        super.viewDidLoad()
        presentWindow = UIApplication.sharedApplication().keyWindow
        baseUrl = defaults.stringForKey("baseUrl")!
        self.myProductTable.delegate = self
        self.myProductTable.dataSource = self
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "My Products"
        self.getMyProductRentRequest()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- TableviewDelegate
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.productList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:MyProductTableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell") as! MyProductTableViewCell
        let  data : MyRentalProduct = self.productList[indexPath.row]
        cell.productImageView.kf_setImageWithURL(NSURL(string: "\(baseUrl)/images/\(data.profileImage.original.path)")!,
                                                 placeholderImage: nil,
                                                 optionsInfo: nil,
                                                 progressBlock: { (receivedSize, totalSize) -> () in
                                                    //  print("Download Progress: \(receivedSize)/\(totalSize)")
            },
                                                 completionHandler: { (image, error, cacheType, imageURL) -> () in
                                                    // print("Downloaded and set!")
            }
        )
        cell.productName.text = data.name
        var cateString : String = ""
        for i in 0 ..< data.productCategories!.count{
            if(i == 0){
                cateString = "\(data.productCategories![i].category.name)"
            }else {
                cateString = "\(cateString), \(data.productCategories![i].category.name)"
            }
        }
        
        
        
        
        cell.category.text = cateString
        
        
        let timeInter1 :NSTimeInterval = data.availableFrom as NSTimeInterval
        let date1 = NSDate(timeIntervalSince1970: timeInter1/1000)
        let timeInter2 :NSTimeInterval = data.availableTill as NSTimeInterval
        let date2 = NSDate(timeIntervalSince1970: timeInter2/1000)
        
        let dayTimePeriodFormatter = NSDateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMM dd YYYY"
        
        
        
        let dateString1 = dayTimePeriodFormatter.stringFromDate(date1)
        let dateString2 = dayTimePeriodFormatter.stringFromDate(date2)
        
        cell.dateRange.text = "\(dateString1) to \(dateString2)"
        cell.accessoryType = .DisclosureIndicator
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       
         let  data : MyRentalProduct = self.productList[indexPath.row]
        print(data.id)
        self.performSegueWithIdentifier("productRentRequest", sender: nil)
        
    }
    
    func acceptAction(sender :AnyObject){
        //  let  req : RentalProduct = self.productList[sender.tag]
        
        
        
    }
    func declineAction(sender: AnyObject)  {
        //let  req : RentalProduct = self.productList[sender.tag]
        
        
    }
    
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
     
        let favorite = UITableViewRowAction(style: .Normal, title: "\u{1F4DD}") { action, index in
            print("favorite button tapped")
        }
       //favorite.backgroundColor = UIColor(netHex:0xD0842D)
     
        let share = UITableViewRowAction(style: .Normal, title: "\u{1F5D1}") { action, index in
            print("share button tapped")
        }
        share.backgroundColor = UIColor.whiteColor()
        
        return [share, favorite]
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // the cells you would like the actions to appear needs to be editable
        return true
    }

    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
         self.getMyProductRentRequest()

    }
    //MARK:- API Access
    func getMyProductRentRequest() {
        if(self.isData == true){
            presentWindow!.makeToastActivity()
            let  paremeters :[String:AnyObject] = ["limit" : 6 , "offset" : self.offset ]
            Alamofire.request(.POST, NSURL(string: "\(baseUrl)api/auth/product/get-my-rental-product" )!, parameters: paremeters)
                //  .validate(contentType: ["application/json"])
                .responseJSON { response in
                    switch response.result {
                    case .Success(let data):
                     //   print(data)
                        let reqRes : MyRentalProductResponse = Mapper<MyRentalProductResponse>().map(data)!
                        //  print(reqRes)
                        if(reqRes.responseStat.status != false){
                            for i in 0 ..< reqRes.responseData!.count{
                                self.productList.append(reqRes.responseData![i])
                                
                            }
                            self.offset += 1
                            self.myProductTable.reloadData()
                            
                        }else{
                            self.isData = false
                            self.view.makeToast(message:"No more data", duration: 2, position: HRToastPositionCenter)
                            
                        }
                        
                        
                        
                    case .Failure(let error):
                        print(error)
                        
                    }
                    self.presentWindow!.hideToastActivity()
            }
            
        }
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
