//
//  PostItemFirstViewController.swift
//  RentGuru
//
//  Created by Workspace Infotech on 8/4/16.
//  Copyright © 2016 Workspace Infotech. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import ObjectMapper


class PostItemFirstViewController: UIViewController ,EPCalendarPickerDelegate{
    @IBOutlet var baseScroll: UIScrollView!
    @IBOutlet var prodCateHolder: UIView!
    @IBOutlet var prodCateLabel: UILabel!
    @IBOutlet var proSubCateHolder: UIView!
    @IBOutlet var prodSubCateLabel: UILabel!
    @IBOutlet var fromDateSelector: UIImageView!
    @IBOutlet var toDateSelector: UIImageView!
    
    @IBOutlet var toDateTxt: UITextField!
    @IBOutlet var fromDateTxt: UITextField!
    @IBOutlet var productTitleTxt: UITextField!
    @IBOutlet var areaText: UIView!
    @IBOutlet var areaTextField: UITextField!
    @IBOutlet var cityTxt: UITextField!
    @IBOutlet var zipTxt: UITextField!
    @IBOutlet var fromDateView: UIView!
    @IBOutlet var toDateView: UIView!
    
    let defaults = NSUserDefaults.standardUserDefaults()
    var baseUrl : String = ""
    
    let cateDropdown = DropDown()
    let subCateDropDown = DropDown()
    let fromCalendarPicker = EPCalendarPicker(startYear: 2016, endYear: 2017, multiSelection: true, selectedDates: [])
    let toCalendarPicker = EPCalendarPicker(startYear: 2016, endYear: 2017, multiSelection: true, selectedDates: [])
    var categoryList : [Category] = []
    var subCategoryList : [Category] = []
    var didTouchedFromDate = false
    var didTouchedToDate = false
    var selectedCategory = Array<Int>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        baseUrl = defaults.stringForKey("baseUrl")!
        
        
        
       
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PostItemFirstViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PostItemFirstViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        
        
        self.navigationController!.navigationBar.barTintColor = UIColor(netHex:0x2D2D2D)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(netHex:0xD0842D)]
        
        self.prodCateHolder.userInteractionEnabled = true
        self.prodCateLabel.userInteractionEnabled = true
        let cateGesture = UITapGestureRecognizer(target: self, action:#selector(PostItemFirstViewController.showHideCateDropDown) )
        self.prodCateHolder.addGestureRecognizer(cateGesture)
        self.prodCateLabel.addGestureRecognizer(cateGesture)
        
        
        self.proSubCateHolder.userInteractionEnabled = true
        self.prodSubCateLabel.userInteractionEnabled = true
        let subCateGesture = UITapGestureRecognizer(target: self, action:#selector(PostItemFirstViewController.showHideSubCateDropDown) )
        self.proSubCateHolder.addGestureRecognizer(subCateGesture)
        self.prodSubCateLabel.addGestureRecognizer(subCateGesture)
        
        self.fromDateSelector.userInteractionEnabled = true
        self.fromDateView.userInteractionEnabled = true
        let fromDateGexture = UITapGestureRecognizer(target: self, action:#selector(PostItemFirstViewController.selectFromDateAction) )
        self.fromDateSelector.addGestureRecognizer(fromDateGexture)
        self.fromDateView.addGestureRecognizer(fromDateGexture)
        
        self.toDateSelector.userInteractionEnabled = true
        self.toDateSelector.userInteractionEnabled = true
        let toDateGesture =  UITapGestureRecognizer(target: self, action:#selector(PostItemFirstViewController.selectToDateAction) )
        self.toDateSelector.addGestureRecognizer(toDateGesture)
        self.toDateView.addGestureRecognizer(toDateGesture)
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.getCategory()
        
        cateDropdown.anchorView = self.prodCateHolder
        subCateDropDown.anchorView = self.prodCateHolder
        
        self.navigationItem.title = "Primary Information"
        
        cateDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            // print("Selected item: \(item) at index: \(index)")
            self.prodCateLabel.attributedText  =  NSAttributedString(string: (item), attributes:[NSForegroundColorAttributeName : UIColor.grayColor()])
            self.prodSubCateLabel.attributedText  =  NSAttributedString(string:"Product Sub Category", attributes:[NSForegroundColorAttributeName : UIColor.grayColor()])
            self.subCategoryList = self.categoryList[index].subcategory
            if(self.subCategoryList.isEmpty){
                self.selectedCategory.append(self.categoryList[index].id)
                 self.subCateDropDown.dataSource = []
            }else{
                var subCates = Array<String>();
                for i in 0  ..< self.subCategoryList.count{
                    subCates.append(self.subCategoryList[i].name)
                }
                self.subCateDropDown.dataSource = subCates
            }
            
            
        }
        cateDropdown.backgroundColor = UIColor.whiteColor()
        cateDropdown.textColor = UIColor.grayColor()
        cateDropdown.selectionBackgroundColor = UIColor(netHex:0xD0842D)
        
        
        subCateDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            // print("Selected item: \(item) at index: \(index)")
            self.prodSubCateLabel.attributedText  =  NSAttributedString(string: (item), attributes:[NSForegroundColorAttributeName : UIColor.grayColor()])
            self.selectedCategory.append(self.subCategoryList[index].id)
        }
        subCateDropDown.backgroundColor = UIColor.whiteColor()
        subCateDropDown.textColor = UIColor.grayColor()
        subCateDropDown.selectionBackgroundColor = UIColor(netHex:0xD0842D)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showHideCateDropDown() {
        cateDropdown.show();
    }
    func showHideSubCateDropDown(){
        subCateDropDown.show()
    }
    @IBAction func nextButonAction(sender: AnyObject) {
        var check = true
        if(self.selectedCategory.isEmpty){
            check = false
            view.makeToast(message:"Category Required", duration: 2, position: HRToastPositionCenter)
            
        }
        if(self.productTitleTxt.text == "" && check != false){
            check = false
            view.makeToast(message:"Title Required", duration: 2, position: HRToastPositionCenter)
        }
        if(self.fromDateTxt.text == "" && check != false)
        {
            check = false
            view.makeToast(message:"Available from date Required", duration: 2, position: HRToastPositionCenter)
        }
        if(self.toDateTxt.text == "" && check != false){
            check = false
            view.makeToast(message:"Available till date Required", duration: 2, position: HRToastPositionCenter)
        }
        if(areaTextField.text == "" && check != false){
            check = false
            view.makeToast(message:"Area Required", duration: 2, position: HRToastPositionCenter)
        }
        if(check != false){
            self .performSegueWithIdentifier("second", sender: nil)
            
        }
       //  self .performSegueWithIdentifier("second", sender: nil)
    }
    
    func keyboardWillShow(sender: NSNotification) {
        let userInfo: [NSObject : AnyObject] = sender.userInfo!
        
        let keyboardSize: CGSize = userInfo[UIKeyboardFrameBeginUserInfoKey]!.CGRectValue.size
        let offset: CGSize = userInfo[UIKeyboardFrameEndUserInfoKey]!.CGRectValue.size
        
        if keyboardSize.height == offset.height {
            if self.view.frame.origin.y == 0 {
                print("if in")
                UIView.animateWithDuration(0.1, animations: { () -> Void in
                    self.view.frame.origin.y -= 140//keyboardSize.height
                })
            }
        } else {
            print("else in")
            UIView.animateWithDuration(0.1, animations: { () -> Void in
                self.view.frame.origin.y += keyboardSize.height - offset.height
            })
        }
        print(self.view.frame.origin.y)
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue()) != nil {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += 140//keyboardSize.height
            }
            else {
                
            }
        }
    }
    func selectFromDateAction()
    {
        
        self.didTouchedFromDate = true;
        fromCalendarPicker.calendarDelegate = self
        fromCalendarPicker.startDate = NSDate()
        fromCalendarPicker.hightlightsToday = true
        fromCalendarPicker.showsTodaysButton = true
        fromCalendarPicker.hideDaysFromOtherMonth = true
        fromCalendarPicker.tintColor = UIColor.orangeColor()
        fromCalendarPicker.multiSelectEnabled = false
        //        calendarPicker.barTintColor = UIColor.greenColor()
        fromCalendarPicker.dayDisabledTintColor = UIColor.grayColor()
        fromCalendarPicker.title = "Available From "
        
        //        calendarPicker.backgroundImage = UIImage(named: "background_image")
        //        calendarPicker.backgroundColor = UIColor.blueColor()
        
        let navigationController = UINavigationController(rootViewController: fromCalendarPicker)
        self.presentViewController(navigationController, animated: true, completion: nil)
    }
    func selectToDateAction()  {
        self.didTouchedToDate = true
        toCalendarPicker.calendarDelegate = self
        toCalendarPicker.startDate = NSDate()
        toCalendarPicker.hightlightsToday = true
        toCalendarPicker.showsTodaysButton = true
        toCalendarPicker.hideDaysFromOtherMonth = true
        toCalendarPicker.tintColor = UIColor.orangeColor()
        toCalendarPicker.multiSelectEnabled = false
        //        calendarPicker.barTintColor = UIColor.greenColor()
        toCalendarPicker.dayDisabledTintColor = UIColor.grayColor()
        toCalendarPicker.title = "Will Available To"
        
        //        calendarPicker.backgroundImage = UIImage(named: "background_image")
        //        calendarPicker.backgroundColor = UIColor.blueColor()
        
        let navigationController = UINavigationController(rootViewController: toCalendarPicker)
        self.presentViewController(navigationController, animated: true, completion: nil)
    }
    // MARK: - EPCalendarPicker
    
    
    func epCalendarPicker(_: EPCalendarPicker, didCancel error : NSError) {
        //if(self == self.fromCalendarPicker)
        //  fromDateTxt.text = "User cancelled selection"
        
    }
    func epCalendarPicker(_: EPCalendarPicker, didSelectDate date : NSDate) {
        
        let formatter = NSDateFormatter()
        formatter.dateFormat = "dd-MM-yyy"
        let str = formatter.stringFromDate(date)
        
        if(self.didTouchedFromDate){
            toCalendarPicker.startDate = date
            self.didTouchedFromDate = false
            fromDateTxt.text = str//"User selected date: \n\(date)"
        }
        if(self.didTouchedToDate){
            self.didTouchedToDate = false
            toDateTxt.text = str
        }
        
        
        
    }
    //    func epCalendarPicker(_: EPCalendarPicker, didSelectMultipleDate dates : [NSDate]) {
    //        fromDateTxt.text = "User selected dates: \n\(dates)"
    //    }
    
    
    
    //MARK : - ApiAccess
    
    
    func  getCategory()  {
        Alamofire.request(.GET, NSURL(string: "\(baseUrl)api/utility/get-category" )!, parameters: [:])
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                //print(response)
                switch response.result {
                case .Success(let data):
                    
                    let cateRes: CategoryResponse = Mapper<CategoryResponse>().map(data)!
                    if((cateRes.responseStat.status) != false){
                        print("data", cateRes.responseData)
                        self.categoryList = cateRes.responseData!
                        var cateSource = Array<String>() ;
                        for i in 0  ..< self.categoryList.count {
                            cateSource.append(self.categoryList[i].name)
                        }
                        self.cateDropdown.dataSource = cateSource
                    }
                case .Failure(let error):
                    print(error)
                }
        }
        
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "second"){
            
            self.navigationController!.navigationBar.barTintColor = UIColor(netHex:0x2D2D2D)
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(netHex:0xD0842D)]
            
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.Plain, target:nil, action:nil)
            self.navigationItem.backBarButtonItem!.tintColor = UIColor.whiteColor()
            let contrller : PostItemSecondViewController = segue.destinationViewController as! PostItemSecondViewController
            contrller.productTitle = self.productTitleTxt.text!
            contrller.selectedCategory = self.selectedCategory
            contrller.address = self.areaTextField.text!
            contrller.city = self.cityTxt.text!
            contrller.zipCode = self.cityTxt.text!
            contrller.availableFrom = self.fromDateTxt.text!
            contrller.availableTill = self.toDateTxt.text!
            
        }
    }
    
    
}
