//
//  PostItemSecondViewController.swift
//  RentGuru
//
//  Created by Workspace Infotech on 8/4/16.
//  Copyright © 2016 Workspace Infotech. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import Photos

class PostItemSecondViewController: UIViewController , UIImagePickerControllerDelegate, UINavigationControllerDelegate ,UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    @IBOutlet var productDescription: UITextView!
    @IBOutlet var baseScroll: UIScrollView!
    @IBOutlet var imageCollection: UICollectionView!
    let imagePicker = UIImagePickerController()
    var imageArray : [UIImage] = []
    var imageToken  = Array<Int>()
    let defaults = NSUserDefaults.standardUserDefaults()
    var baseUrl : String = ""
    var UploadCounter = 0
    var filenames :[String] = []
    //From Previous Controller
    
    var selectedCategory :[Int]!
    var productTitle: String!
    var availableFrom : String!
    var availableTill :String!
    var address :String!
    var city: String!
    var zipCode : String!
    
    
    
    var screenSize: CGRect!
    var screenWidth: CGFloat!
    var screenHeight: CGFloat!
    
    var presentWindow : UIWindow?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // self.tabBarController!.tabBar.hidden = true;
        self.hideKeyboardWhenTappedAround()
        imagePicker.delegate = self
        presentWindow = UIApplication.sharedApplication().keyWindow
        screenSize = UIScreen.mainScreen().bounds
        screenWidth = screenSize.width
        screenHeight = screenSize.height
        baseUrl = defaults.stringForKey("baseUrl")!
        
        
        
        self.imageCollection.delegate = self
        self.imageCollection.dataSource = self
        
        
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        
        self.navigationItem.title = "Details"
        let button = UIButton(type: UIButtonType.System) as UIButton
        //button.setImage(UIImage(named: "back.png"), forState: UIControlState.Normal)
        button.setTitle("Next", forState:UIControlState.Normal)
        //.attributedText = NSAttributedString(string: "Next", attributes:[NSForegroundColorAttributeName : UIColor.lightGrayColor()])
        button.tintColor = UIColor.whiteColor()
        button.addTarget(self, action:#selector(PostItemSecondViewController.performNext), forControlEvents: UIControlEvents.TouchUpInside)
        button.frame=CGRectMake(0, 0, 35, 35)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        
        
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
        layout.itemSize = CGSize(width: screenWidth/3, height: screenWidth/3)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        // layout.minimumInteritemSpacing = 0
        self.imageCollection.collectionViewLayout = layout
        
    }
    override func viewDidLayoutSubviews() {
        self.baseScroll.contentSize = CGSizeMake(
            self.view.frame.size.width,
            self.view.frame.size.height + 206
        );
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func performNext(){
        var checkAll = true
        if(self.productDescription.text == ""){
            checkAll = false
            view.makeToast(message:"product description Required", duration: 2, position: HRToastPositionCenter)
        }
        if(self.imageArray.isEmpty && checkAll != false){
            checkAll = false
            view.makeToast(message:"add some images ", duration: 2, position: HRToastPositionCenter)
            
        }
        if(checkAll != false){
            self.uploadFiles()
        }
        //  self.performSegueWithIdentifier("third", sender: nil)
    }
    
    
    @IBAction func chooseimageAction(sender: AnyObject) {
        if(self.imageArray.count<5){
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .PhotoLibrary
            
            presentViewController(imagePicker, animated: true, completion: nil)
        }else{
            view.makeToast(message:"maximum 5 images are allowed", duration: 2, position: HRToastPositionCenter)
        }
        
    }
    func uploadFiles(){
        presentWindow!.makeToastActivity()
        let fileUploader = FileUploader()
        fileUploader.addFileData( UIImageJPEGRepresentation(self.imageArray[self.UploadCounter],0.8)!, withName: "productImage", withMimeType: "image/jpeg" ,withFileName: filenames[self.UploadCounter] )
        let request = NSMutableURLRequest( URL: NSURL(string: "\(baseUrl)fileupload/upload/product-image" )! )
        request.HTTPMethod = "POST"
        fileUploader.uploadFile(request: request)!
            .progress { [weak self] bytesWritten, totalBytesWritten, totalBytesExpectedToWrite in
                //  To update your ui, dispatch to the main queue.
                dispatch_async(dispatch_get_main_queue()) {
                    print("Total bytes written on main queue: \(totalBytesWritten)....\(totalBytesExpectedToWrite)")
                }
            }
            .responseJSON { [weak self] response in
                switch response.result {
                case .Success(let data):
                    print(data)
                    let idenDocRes: IdentityDocumentResponse = Mapper<IdentityDocumentResponse>().map(data)!
                    
                    if(idenDocRes.responseStat.status != false){
                        self!.imageToken.append(idenDocRes.responseData)
                        self!.UploadCounter += 1
                        if(self?.imageArray.count >  self?.UploadCounter){
                            self?.uploadFiles()
                        }
                        else{
                            self!.presentWindow!.hideToastActivity()
                            self!.performSegueWithIdentifier("third", sender:nil)
                        }
                    }
                    
                case .Failure(let error):
                    print("Request failed with error: \(error)")
                }
        }
        
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            print("picked image :\(pickedImage)")
            self.imageArray.append(pickedImage)
            self.imageCollection.reloadData()
            
        }
        if let imageURL = info[UIImagePickerControllerReferenceURL] as? NSURL {
            let result = PHAsset.fetchAssetsWithALAssetURLs([imageURL], options: nil)
            let filename = result.firstObject?.filename ?? ""
            print(filename)
            
            self.filenames.append(filename!)
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Collection View Delegate
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return   self.imageArray.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as!SelectedImageCollectionViewCell
        
        // Configure the cell
        cell.frame.size.width = screenWidth / 3
        cell.frame.size.height = screenWidth / 3
        
        cell.imageView.image = imageArray[indexPath.row]
        
        return cell
        
        
        
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        //        if indexPath.row == 0
        //        {
        //            return CGSize(width: screenWidth, height: screenWidth/3)
        //        }
        return CGSize(width: screenWidth/3, height: screenWidth/3);
        
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        
        if(segue.identifier == "third"){
            
            
            
            let contrller : PostItemThirdViewController = segue.destinationViewController as! PostItemThirdViewController
            contrller.productTitle = self.productTitle
            contrller.selectedCategory = self.selectedCategory
            contrller.address = self.address
            contrller.city = self.city
            contrller.zipCode = self.zipCode
            contrller.availableFrom = self.availableFrom
            contrller.availableTill = self.availableTill
            contrller.Productdescription = self.productDescription.text!
            contrller.imageTokenArray  = self.imageToken
            
            
            self.navigationController!.navigationBar.barTintColor = UIColor(netHex:0x2D2D2D)
            self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(netHex:0xD0842D)]
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.Plain, target:nil, action:nil)
        }
    }
    
    
    
}
