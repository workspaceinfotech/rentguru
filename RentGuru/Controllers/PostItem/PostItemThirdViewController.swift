//
//  PostItemThirdViewController.swift
//  RentGuru
//
//  Created by Workspace Infotech on 8/11/16.
//  Copyright © 2016 Workspace Infotech. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import ObjectMapper

class PostItemThirdViewController: UIViewController {
    
    @IBOutlet var dropDownImage: UIImageView!
    @IBOutlet var dropDownLabel: UILabel!
    @IBOutlet var dropDownContainer: UIView!
    @IBOutlet var rentFee: UITextField!
    @IBOutlet var currentPrice: UITextField!
    let dropDown =  DropDown()
    let defaults = NSUserDefaults.standardUserDefaults()
    var baseUrl : String = ""
    var rentTypeList : [RentType]!
    var rentTypeId : Int!
    var selectedCategory : [Int]!
    var productTitle: String!
    var availableFrom: String!
    var availableTill :String!
    var address :String!
    var city: String!
    var zipCode : String!
    var Productdescription: String!
    var imageTokenArray : [Int]!
    var rentType : Int!
    var presentWindow : UIWindow?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presentWindow = UIApplication.sharedApplication().keyWindow
        self.hideKeyboardWhenTappedAround()
        baseUrl = defaults.stringForKey("baseUrl")!
        self.dropDownImage.userInteractionEnabled = true
        self.dropDownContainer.userInteractionEnabled = true
        let subCateGesture = UITapGestureRecognizer(target: self, action:#selector(PostItemThirdViewController.showHideDropDown) )
        self.dropDownContainer.addGestureRecognizer(subCateGesture)
        self.dropDownImage.addGestureRecognizer(subCateGesture)
    }
    override func viewWillAppear(animated: Bool) {
        self.navigationItem.title = "Priceing"
        let button = UIButton(type: UIButtonType.System) as UIButton
        //button.setImage(UIImage(named: "back.png"), forState: UIControlState.Normal)
        button.setTitle("Done", forState:UIControlState.Normal)
        button.tintColor = UIColor.whiteColor()
        button.addTarget(self, action:#selector(PostItemThirdViewController.submitProduct), forControlEvents: UIControlEvents.TouchUpInside)
        button.frame=CGRectMake(0, 0, 40, 40)
        let barButton = UIBarButtonItem(customView: button)
        
        self.navigationItem.rightBarButtonItem = barButton
        
        dropDown.anchorView = self.dropDownContainer
        // dropDown.dataSource = ["Daily", "Weekly","Monthly"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            // print("Selected item: \(item) at index: \(index)")
            self.dropDownLabel.attributedText  =  NSAttributedString(string: (item), attributes:[NSForegroundColorAttributeName : UIColor.grayColor()])
            self.rentTypeId = self.rentTypeList[index].id
        }
        dropDown.backgroundColor = UIColor.whiteColor()
        dropDown.textColor = UIColor.grayColor()
        dropDown.selectionBackgroundColor = UIColor(netHex:0xD0842D)
        self.getRentType()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func showHideDropDown() {
        print( "Hello")
        dropDown.show();
    }
    
    func submitProduct() {
        var checkAll = true
        
        if(self.currentPrice.text == ""){
            checkAll = false
            view.makeToast(message:"current market value is required", duration: 2, position: HRToastPositionCenter)
        }
        if(self.rentFee.text == "" && checkAll != false){
            checkAll = false
            view.makeToast(message:"rent fee is required", duration: 2, position: HRToastPositionCenter)
        }
        if(self.rentTypeId == nil && checkAll != false){
            checkAll = false
            view.makeToast(message:"rent fee for is  required", duration: 2, position: HRToastPositionCenter)
        }
        if(checkAll != false){
            presentWindow!.makeToastActivity()
            
            var tokenArray:[Int] = []
            for i in 0  ..< self.imageTokenArray.count {
                if(i != 0 ){
                    tokenArray.append(self.imageTokenArray![i])
                }
            }
            
            let paremeters : [String :AnyObject] =
                ["name": self.productTitle!,"description":self.Productdescription! ,"profileImageToken" :self.imageTokenArray![0] ,"otherImagesToken" :"\(tokenArray)","currentValue" : self.currentPrice.text!,"rentFee" : self.rentFee.text!,"availableFrom" : self.availableFrom! ,"availableTill" : self.availableTill! , "categoryIds" :"\(self.selectedCategory)" , "formattedAddress" : self.address!,"zip" : self.zipCode! ,"city" : self.city!,"rentTypeId" : self.rentTypeId!]
            print(paremeters)
            Alamofire.request(.POST, NSURL(string: "\(baseUrl)api/auth/product/upload" )!, parameters: paremeters)
                .validate(contentType: ["application/json"])
                .responseJSON { response in
                    print(response)
                    self.presentWindow!.hideToastActivity()
                    switch response.result {
                    case .Success(let data):
                        
                        let productRes: PostProductResponse = Mapper<PostProductResponse>().map(data)!
                        if((productRes.responseStat.status) != false){
                            
                            self.view.makeToast(message:"Product Posted successfully", duration: 2, position: HRToastPositionCenter)
                            self.currentPrice.text = ""
                            self.rentFee.text = ""
                            if let tabBarController = self.presentWindow!.rootViewController as? UITabBarController {
                                tabBarController.selectedIndex = 1
                            }
                            
                        }else{
                            self.view.makeToast(message:productRes.responseStat.requestErrors![0].msg, duration: 2, position: HRToastPositionCenter)
                        }
                    case .Failure(let error):
                        print(error)
                    }
            }
            
            //
        }
    }
    
    func  getRentType()  {
        Alamofire.request(.GET, NSURL(string: "\(baseUrl)api/utility/get-rent-type" )!, parameters: [:])
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                //print(response)
                switch response.result {
                case .Success(let data):
                    
                    let rentTypeRes: RentTypeResponse = Mapper<RentTypeResponse>().map(data)!
                    if((rentTypeRes.responseStat.status) != false){
                        
                        self.rentTypeList = rentTypeRes.responseData!
                        var cateSource = Array<String>() ;
                        for i in 0  ..< self.rentTypeList.count {
                            cateSource.append(self.rentTypeList[i].name)
                        }
                        self.dropDown.dataSource = cateSource
                    }
                case .Failure(let error):
                    print(error)
                }
        }
        
    }
}
